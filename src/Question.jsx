import { useState } from 'react';

export default function Question({ id, text, answers, onAnswer }) {
  const [selectedAnswer, setSelectedAnswer] = useState(null);
  const [answered, setAnswered] = useState(false);

  function handleAnswer(answer) {
    setSelectedAnswer(answer);
    onAnswer(answer.isCorrect);
    setAnswered(true);
  }

  return (
    <div className="question-container">
      <h5>{text}</h5>
      <div>
        {answers.map((answer) => (
          <label key={answer.text}>
            <input
              disabled={answered}
              onChange={() => handleAnswer(answer)}
              name={id}
              type="radio"
            />
            {answer.text}
          </label>
        ))}
      </div>

      {selectedAnswer && selectedAnswer.isCorrect ? (
        <small style={{ color: 'green' }}>Correct answer</small>
      ) : null}

      {selectedAnswer && !selectedAnswer.isCorrect ? (
        <small style={{ color: 'red' }}>Incorrect answer</small>
      ) : null}
    </div>
  );
}
