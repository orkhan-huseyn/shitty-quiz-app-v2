import { useState } from 'react';
import questions from './data';
import Question from './Question';

function App() {
  const [correctAnswers, setCorrectAnswers] = useState(0);

  function handleAnswer(isCorrect) {
    if (isCorrect) {
      setCorrectAnswers((a) => a + 1);
    }
  }

  return (
    <div>
      <h1>Quiz App!</h1>
      <p>Correct answers {correctAnswers}</p>
      <hr />
      {questions.map((question) => (
        <Question
          key={question.id}
          id={question.id}
          text={question.text}
          answers={question.answers}
          onAnswer={handleAnswer}
        />
      ))}
    </div>
  );
}

export default App;
