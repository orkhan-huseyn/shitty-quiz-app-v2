import questions from './questions.json';

export default questions.map((question) => {
  const {
    id,
    question: text,
    answers,
    correct_answers,
    multiple_correct_answers,
    difficulty,
  } = question;

  return {
    id,
    text,
    difficulty,
    multiple: multiple_correct_answers === 'true',
    answers: Object.entries(answers)
      .filter((entry) => entry[1] !== null)
      .map((entry) => {
        const [key, value] = entry;
        const correctAnswerKey = key + '_correct';
        const isCorrect = correct_answers[correctAnswerKey];

        return {
          text: value,
          isCorrect: isCorrect === 'true',
        };
      }),
  };
});
